import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      //backgroundColor: Colors.grey,
      body: Center(
        child: Container(
          // // padding:
          // //     const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
          // height: 200,
          // width: 200,
          // // //padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          // color: Colors.red,

          // margin:
          //     const EdgeInsets.only(top: 10, right: 10, bottom: 10, left: 10),

          height: 300,
          width: 300,
          decoration: const BoxDecoration(
              color: Colors.amber,
              borderRadius: BorderRadius.all(
                Radius.circular(20),
              ),
              // border: Border.all(
              //   color: null,
              //   width: 0,
              // ),
              gradient: LinearGradient(
                  stops: [0.1, 1], colors: [Colors.red, Colors.green]),
              boxShadow: [
                BoxShadow(
                    color: Colors.purple,
                    offset: Offset(30, 30),
                    blurRadius: 8),
                BoxShadow(
                    color: Colors.red, offset: Offset(20, 20), blurRadius: 8),
                BoxShadow(
                    color: Colors.green, offset: Offset(10, 10), blurRadius: 8),
                BoxShadow(
                    color: Colors.purple,
                    offset: Offset(-30, -30),
                    blurRadius: 8),
                BoxShadow(
                    color: Colors.red, offset: Offset(-20, -20), blurRadius: 8),
                BoxShadow(
                    color: Colors.green,
                    offset: Offset(-10, -10),
                    blurRadius: 8),
              ]),
        ),
      ),
    ));
  }
}
